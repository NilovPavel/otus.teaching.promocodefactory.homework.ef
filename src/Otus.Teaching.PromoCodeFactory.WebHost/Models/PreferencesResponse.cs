﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{

    public class PreferencesResponse
    {
        public IEnumerable<Preference> Preferences
        { get; set; }
    }
}
