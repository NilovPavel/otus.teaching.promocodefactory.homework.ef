﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<BaseEntity> _repository;
        public CustomersController(IRepository<BaseEntity> repository)
        { 
            this._repository = repository;
        } 

        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            //TODO: Добавить получение списка клиентов

            Customer customer = (Customer)(await this._repository.GetAllAsync()).FirstOrDefault();

            if (customer == null)
                return NotFound();

            CustomerShortResponse response = new CustomerShortResponse
            { Id = customer.Id, FirstName = customer.FirstName, LastName = customer.LastName, Email = customer.Email };

            return Ok(response);
        }
        
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            //TODO: Добавить получение клиента вместе с выданными ему промомкодами
            Customer customer = await this._repository.GetByIdAsync(id) as Customer;

            if (customer == null)
                return NotFound();

            CustomerResponse response = new CustomerResponse
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
                PromoCodes = customer.PromoCode == null ? new List<PromoCodeShortResponse>()
                : new List<PromoCodeShortResponse>()
                {
                    new PromoCodeShortResponse
                    {
                        Id = customer.PromoCode.Id,
                        BeginDate = customer.PromoCode.BeginDate.ToString(),
                        EndDate = customer.PromoCode.EndDate.ToString(),
                        Code = customer.PromoCode.Code,
                        PartnerName = customer.PromoCode.PartnerName,
                        ServiceInfo = customer.PromoCode.ServiceInfo
                    }
                }
            };

            return Ok(response);
        }

        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            //TODO: Добавить создание нового клиента вместе с его предпочтениями
            Customer customer = new Customer
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
            };
            
            Guid guid = await this._repository.AddNewCustomer(customer, request.PreferenceIds);
            
            return Ok(guid);
        }
        
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            Customer customer = new Customer
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
            };

            Customer editCustomer = (await this._repository.EditCustomer(id, customer, request.PreferenceIds)) as Customer;
            
            if (editCustomer == null)
                return NotFound();

            return Ok(editCustomer);
        }
        
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            //TODO: Удаление клиента вместе с выданными ему промокодами
            bool isSuccess = await this._repository.DeleteCustomer(id);

            if (!isSuccess)
                return NotFound(id); 

            return Ok("Пользователь с id = " + id + " удален.");
        }
    }
}