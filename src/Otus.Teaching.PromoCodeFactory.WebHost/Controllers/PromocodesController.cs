﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<BaseEntity> _repository;

        public PromocodesController(IRepository<BaseEntity> repository)
        {
            this._repository = repository;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            //TODO: Получить все промокоды 
            List<PromoCodeShortResponse> promoCodeShortResponses = new List<PromoCodeShortResponse>();

            IEnumerable<PromoCode> promoCodes = await this._repository.GetAllPromocodes() as IEnumerable<PromoCode>;

            foreach (var promoCode in promoCodes)
            {
                promoCodeShortResponses.Add(new PromoCodeShortResponse 
                {
                    Id = promoCode.Id,
                    BeginDate = promoCode.BeginDate.ToLongDateString(),
                    Code = promoCode.Code,
                    EndDate = promoCode.EndDate.ToLongDateString(),
                    PartnerName = promoCode.PartnerName,
                    ServiceInfo = promoCode.ServiceInfo
                });
            }

            return Ok(promoCodeShortResponses);
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns>Возвращаемое значение добавленный промокод </returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            //TODO: Создать промокод и выдать его клиентам с указанным предпочтением
            PromoCode promoCode = new PromoCode
            {
                ServiceInfo = request.ServiceInfo,
                PartnerName = request.PartnerName,
                Code = request.PromoCode,
                PreferenceId = Guid.Parse(request.Preference)
            };

            PromoCode addedPromoCode = await this._repository.AddNewPromocode(promoCode) as PromoCode;

            if (addedPromoCode == null) 
                return BadRequest();

            return Ok(addedPromoCode);
        }
    }
}