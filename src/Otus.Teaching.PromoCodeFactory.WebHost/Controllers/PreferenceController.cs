﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Net.Mime;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения пользователей
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferenceController : ControllerBase
    {
        private readonly IRepository<BaseEntity> _repository;

        public PreferenceController(IRepository<BaseEntity> repository)
        {
            this._repository = repository;
        }



        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns>Возвращает коллекцию объектов типа Preference</returns>
        [HttpGet]
        public Task<ActionResult<PreferencesResponse>> GetPreferencesAsync()
        {
            IEnumerable<Preference> preferences = (IEnumerable<Preference>)this._repository.GetAllPreferences().Result;

            PreferencesResponse response = new PreferencesResponse
            { Preferences = preferences };

            return Task.FromResult(new ActionResult<PreferencesResponse>(response));
        }

        /// <summary>
        /// Получает промокод по Id
        /// </summary>
        /// <param name="id">Идентификатор предпочтения</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public Task<ActionResult<PreferenceResponse>> GetPreferencesAsync(Guid id)
        {
            Preference preference = (Preference)this._repository.GetPreferenceById(id).Result;

            PreferenceResponse response = new PreferenceResponse
            { Preference = preference };

            return Task.FromResult(new ActionResult<PreferenceResponse>(response));
        }
    }
}
