﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Context
{
    public static class EntityFrameworkConfigurator
    {
        public static IServiceCollection ConfigureContext(this IServiceCollection services)
        {
            string connectionString = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build().GetConnectionString("DefaultConnection");

            DbContextOptionsBuilder<EfContext> optionsBuilder = 
                new DbContextOptionsBuilder<EfContext>();

            DbContextOptions<EfContext> options = 
                optionsBuilder.UseSqlite(connectionString).Options;

            services.AddSingleton(typeof(DbContext), new EfContext(options));//.AddDbContext<EfContext>(options);

            return services;
        }
    }
}
