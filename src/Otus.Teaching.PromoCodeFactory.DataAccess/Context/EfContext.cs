﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Context
{
    public class EfContext : DbContext
    {
        public EfContext(DbContextOptions<EfContext> options) : base(options)
        {
            Database.EnsureDeleted();
            Database.EnsureCreated();
            this.WriteFake();
        }

        private void WriteFake()
        {
            this.Employees.AddRange(FakeDataFactory.Employees);
            this.Roles.AddRange(FakeDataFactory.Roles.Where(item => 
                !FakeDataFactory.Employees.Select( empl => empl.Role.Id).Contains(item.Id)));

            this.Preferences.AddRange(FakeDataFactory.Preferences);
            this.Customers.AddRange(FakeDataFactory.Customers);
            
            this.PromoCodes.AddRange(FakeDataFactory.PromoCodes);
            this.CustomerPreferences.AddRange(FakeDataFactory.CustomerPreferences);
            
            this.SaveChanges();
        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<CustomerPreference> CustomerPreferences { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }
        public DbSet<Role> Roles { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }
    }
}
