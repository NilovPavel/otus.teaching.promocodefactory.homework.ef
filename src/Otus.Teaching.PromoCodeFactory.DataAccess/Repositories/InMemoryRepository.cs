﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<Guid> AddNewCustomer(Customer customer, List<Guid> preferenceIds)
        {
            throw new NotImplementedException();
        }

        public Task<T> EditCustomer(Guid id, Customer customer, List<Guid> preferenceIds)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<T>> GetAllPreferences()
        {
            throw new NotImplementedException();
        }

        public Task<T> GetPreferenceById(Guid id)
        {
            throw new NotImplementedException();
        }

        Task<bool> IRepository<T>.DeleteCustomer(Guid id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<T>> GetAllPromocodes()
        {
            throw new NotImplementedException();
        }

        public Task<T> AddNewPromocode(PromoCode promoCode)
        {
            throw new NotImplementedException();
        }
    }
}