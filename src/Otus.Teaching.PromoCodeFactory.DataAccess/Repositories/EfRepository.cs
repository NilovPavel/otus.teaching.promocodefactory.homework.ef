﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T>
        where T : BaseEntity
    {
        private EfContext dataContext;
        public EfRepository(DbContext dataContext) 
        {
            this.dataContext = dataContext as EfContext;

            if (this.dataContext == null) 
                throw new Exception("Используемый в репозитории контекст данных не является допустимым.");
        }

        public Task<Guid> AddNewCustomer(Customer customer, List<Guid> preferenceIds)
        {
            Customer addedCustomer = this.dataContext.Customers.Add(customer).Entity;

            IEnumerable<Preference> preferences = this.dataContext.Preferences.Where(item => preferenceIds.Contains(item.Id));

            foreach (Guid preferenceId in preferenceIds)
                this.dataContext.CustomerPreferences.Add(new CustomerPreference { CustomerId = addedCustomer.Id, PreferenceId = preferenceId });

            this.dataContext.SaveChanges();

            return Task.FromResult(addedCustomer.Id);
        }

        public Task<T> AddNewPromocode(PromoCode promoCode)
        {
            Employee currentEmployee = this.dataContext.Employees
                .FirstOrDefault(item => item.FirstName.Equals(promoCode.PartnerName));

            promoCode.EmployeeId = currentEmployee.Id;

            PromoCode addedPromoCode = this.dataContext.PromoCodes.Add(promoCode).Entity;

            IEnumerable<Guid> userGuides = this.dataContext.CustomerPreferences
                .Where(item => item.PreferenceId.Equals(promoCode.PreferenceId)).Select(item2 => item2.CustomerId);

            foreach (Customer customer in this.dataContext.Customers.Where(item => userGuides.Contains(item.Id)))
                customer.PromoCode = promoCode;

            this.dataContext.SaveChanges();

            return Task.FromResult(addedPromoCode as T);
        }

        public Task<T> EditCustomer(Guid id, Customer customer, List<Guid> preferenceIds)
        {
            Customer editedCustomer = this.dataContext.Customers.FirstOrDefault(item => item.Id.Equals(id));

            if (editedCustomer == null)
                return Task.FromResult(editedCustomer as T);

            editedCustomer.FirstName = customer.FirstName;
            editedCustomer.LastName = customer.LastName;
            editedCustomer.Email = customer.Email;

            foreach (Guid preferenceId in preferenceIds)
                this.dataContext.CustomerPreferences.Add(new CustomerPreference { CustomerId = editedCustomer.Id, PreferenceId = preferenceId });

            this.dataContext.SaveChanges();

            return Task.FromResult(editedCustomer as T);
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(this.dataContext.Customers.ToList() as IEnumerable<T>);
        }

        public Task<IEnumerable<T>> GetAllPreferences()
        {
            return Task.FromResult(this.dataContext.Preferences.ToList() as IEnumerable<T>);
        }

        public Task<IEnumerable<T>> GetAllPromocodes()
        {
            return Task.FromResult(this.dataContext.PromoCodes.ToList() as IEnumerable<T>);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(this.dataContext.Customers.FirstOrDefault(item => item.Id.Equals(id)) as T);
        }

        public Task<T> GetPreferenceById(Guid id)
        {
            T reference = this.dataContext.Preferences.FirstOrDefault(item => item.Id.Equals(id)) as T;
            return Task.FromResult(this.dataContext.Preferences.FirstOrDefault(item => item.Id.Equals(id)) as T);
        }

        Task<bool> IRepository<T>.DeleteCustomer(Guid id)
        {
            Customer editedCustomer = this.dataContext.Customers.FirstOrDefault(item => item.Id.Equals(id));

            if (editedCustomer == null)
                return Task.FromResult(false);

            this.dataContext.Remove(editedCustomer);
            this.dataContext.SaveChanges();

            return Task.FromResult(true);
        }
    }
}
