﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    /// <summary>
    /// Предпочтения
    /// </summary>
    public class Preference
        : BaseEntity
    {
        /// <summary>
        /// Id предпочтения
        /// </summary>
        public int IdPreference { get; set; }
        /// <summary>
        /// Наименование предпочтения
        /// </summary>
        [MaxLength(100)]
        public string Name { get; set; }
    }
}