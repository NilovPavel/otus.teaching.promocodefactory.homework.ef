﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();
        Task<T> GetByIdAsync(Guid id);
        Task<Guid> AddNewCustomer(Customer customer, List<Guid> preferenceIds);
        Task<T> EditCustomer(Guid id, Customer customer, List<Guid> preferenceIds);
        Task<bool> DeleteCustomer(Guid id);
        Task<IEnumerable<T>> GetAllPreferences();
        Task<T> GetPreferenceById(Guid id);
        Task<IEnumerable<T>> GetAllPromocodes();
        Task<T> AddNewPromocode(PromoCode promoCode);
    }
}